package com.balasasidhar.payumoneysdkmodule;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by sasidhar on 08/02/18.
 */

public interface APIService {
    @POST("payment/hash")
    Call<HashResponse> generateHash(@Body HashData data);
}
