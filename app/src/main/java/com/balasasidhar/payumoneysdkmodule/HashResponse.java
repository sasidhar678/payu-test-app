package com.balasasidhar.payumoneysdkmodule;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sasidhar on 08/02/18.
 */

public class HashResponse {

    private String key;
    private String txnid;
    private String productinfo;
    private String merchant_id;
    @SerializedName("request_hash")
    private String requestHash;

    public HashResponse() {
    }

    public HashResponse(String key, String txnid, String productinfo, String requestHash, String merchant_id) {
        this.key = key;
        this.txnid = txnid;
        this.productinfo = productinfo;
        this.requestHash = requestHash;
        this.merchant_id = merchant_id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getTxnid() {
        return txnid;
    }

    public void setTxnid(String txnid) {
        this.txnid = txnid;
    }

    public String getProductinfo() {
        return productinfo;
    }

    public void setProductinfo(String productinfo) {
        this.productinfo = productinfo;
    }

    public String getRequestHash() {
        return requestHash;
    }

    public void setRequestHash(String requestHash) {
        this.requestHash = requestHash;
    }

    public String getMerchant_id() {
        return merchant_id;
    }

    public void setMerchant_id(String merchant_id) {
        this.merchant_id = merchant_id;
    }
}
