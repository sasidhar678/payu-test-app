package com.balasasidhar.payumoneysdkmodule;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.payumoney.core.PayUmoneySdkInitializer;
import com.payumoney.core.entity.TransactionResponse;
import com.payumoney.sdkui.ui.utils.PayUmoneyFlowManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }

    public void startPayment(View view) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://192.168.2.5:3000/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIService apiService = retrofit.create(APIService.class);

        HashData hashData = new HashData("9959582678", "Sasidhar",
                "sasidhar.678@gmail.com", 10.25);

        Call<HashResponse> call = apiService.generateHash(hashData);

        call.enqueue(new Callback<HashResponse>() {
            @Override
            public void onResponse(@NonNull Call<HashResponse> call, @NonNull Response<HashResponse> response) {
                if (response.isSuccessful()) {
                    sendPaymentRequest(response.body());
                }
            }

            @Override
            public void onFailure(@NonNull Call<HashResponse> call, @NonNull Throwable t) {

            }
        });
    }

    private void sendPaymentRequest(HashResponse response) {


        PayUmoneySdkInitializer.PaymentParam.Builder builder = new
                PayUmoneySdkInitializer.PaymentParam.Builder();
        builder.setAmount(10.00)
                .setTxnId(response.getTxnid())
                .setPhone("9959582678")
                .setProductName(response.getProductinfo())
                .setFirstName("Sasidhar")
                .setEmail("sasidhar.678@gmail.com")
                .setsUrl("https://www.payumoney.com/mobileapp/payumoney/success.php")
                .setfUrl("https://www.payumoney.com/mobileapp/payumoney/failure.php")
                .setUdf1("")
                .setUdf2("")
                .setUdf3("")
                .setUdf4("")
                .setUdf5("")
                .setIsDebug(true)
                .setKey(response.getKey())
                .setMerchantId(response.getMerchant_id());

        //declare paymentParam object
        PayUmoneySdkInitializer.PaymentParam paymentParam = builder.build();
        //set the hash
        paymentParam.setMerchantHash(response.getRequestHash());

        // Invoke the following function to open the checkout page.
        PayUmoneyFlowManager.startPayUMoneyFlow(paymentParam, this,
                R.style.AppTheme_NoActionBar, true);
    }


}
